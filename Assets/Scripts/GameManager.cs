﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    public int levelGame;
    public int[] countCell;
    public GameObject cellPref;
    public Transform cellParent;
    [Space(20)]
    public List<GameObject> cells = new List<GameObject>();
    public List<int> usedStates = new List<int>();
    public List<int> usedSprites = new List<int>();
    [Space(20)]
    public float fadeDuration;
    public Text findTxt;
    public Ease easeAnimTxt;
    [Space(20)]
    public GameObject restartMenu;
    public GameObject eclipse;
    [Space(20)]
    public bool isRest = true;
    [Space(20)]
    public List<Sprite> sprites = new List<Sprite>();
    void Start()
    {
        GM = this;
    }
    void Update()
    {
        if (isRest)
        {
            findTxt.GetComponent<FadeController>().FadeIn(0, 1, fadeDuration);
            levelGame = 0;
            restartMenu.gameObject.SetActive(false);
            CreateCells();
            isRest = false;
        }
    }
    public void CreateCells()
    {
        int tempCountCell = countCell[levelGame];
        for (int i = 0; i < tempCountCell; i++)
        {
            GameObject tempCell = Instantiate(cellPref);
            tempCell.transform.SetParent(cellParent, false);
            cells.Add(tempCell);
        }
        int rnd = Random.Range(0, cells.Count);
        cells[rnd].GetComponent<Cell>().state = 1;
    }
    public void RestGame()
    {
        LoadingScreen.LS.Load();
    }
}
