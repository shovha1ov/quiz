﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen LS;
    public float timeLoad;
    public float timeLoadLoadFadeOut;
    public float fadeDuration;
    public GameObject loadScreen;
    void Start()
    {
        LS = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Load()
    {
        loadScreen.SetActive(true);
        loadScreen.GetComponent<FadeController>().FadeIn(0.6f, 1, fadeDuration);
        GameManager.GM.restartMenu.gameObject.SetActive(false);
        StartCoroutine(LoadFadeOut());
    }
    IEnumerator LoadFadeOut()
    {
        StartCoroutine(LoadScreen());
        yield return new WaitForSeconds(timeLoadLoadFadeOut);
        loadScreen.GetComponent<FadeController>().FadeOut(1, 0, fadeDuration);
    }
    IEnumerator LoadScreen()
    {
        yield return new WaitForSeconds(timeLoad);
        loadScreen.SetActive(false);
        GameManager.GM.usedStates.Clear();
        GameManager.GM.usedSprites.Clear();
        GameManager.GM.levelGame = 0;
        GameManager.GM.findTxt.gameObject.SetActive(true);
        GameManager.GM.isRest = true;
    }
}
