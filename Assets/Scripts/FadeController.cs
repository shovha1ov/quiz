﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{
    public static FadeController FC;
    void Start()
    {
        FC = this;   
    }
    public void FadeIn(float startOpacity, float endOpacity, float duration)
    {
        Fade(startOpacity, endOpacity, duration);
    }
    public void FadeOut(float startOpacity, float endOpacity, float duration)
    {
        Fade(startOpacity, endOpacity, duration);
    }
    public void Fade(float startOpacity, float endOpacity, float duration)
    {
        var graphics = gameObject.GetComponentsInChildren<Graphic>();
        foreach (var graphic in graphics)
        {
            var startColor = graphic.color;
            startColor.a = startOpacity;
            graphic.color = startColor;
            
            var endColor = graphic.color;
            endColor.a = endOpacity;
            graphic.DOColor(endColor, duration);
        }
    }
}
