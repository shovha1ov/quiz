﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    public static Cell CL;
    public int state;  
    public GameObject child;
    [Space(20)]
    public Ease animEase;
    public float animDuration;
    [Space(20)]
    public Ease animEaseSpawn;
    public float animDurationSpawn;
    [Space(20)]
    public AudioSource myFx;
    public AudioClip spawnFx;
    [Space(20)]
    public GameObject particle;
    
    void Start()
    {
        CL = this;
        if (GameManager.GM.levelGame == 0)
        {
            AnimSpawnCell();
        }
        int tempIdSprite = 0;
        if (state == 1)
        {
            bool checkState = true;
            while (checkState)
            {
                tempIdSprite = Random.Range(0, GameManager.GM.sprites.Count);
                if (!GameManager.GM.usedStates.Contains(tempIdSprite))
                {
                    if (!GameManager.GM.usedSprites.Contains(tempIdSprite))
                        checkState = false;
                }
            }
        }
        else
        {
            bool checkSprite = true;
            while (checkSprite)
            {
                tempIdSprite = Random.Range(0, GameManager.GM.sprites.Count);
                if (!GameManager.GM.usedSprites.Contains(tempIdSprite))
                {
                    checkSprite = false;
                }
            }
        }     
        Sprite sprite = GameManager.GM.sprites[tempIdSprite];
        child.gameObject.GetComponent<Image>().sprite = sprite;
        GameManager.GM.usedSprites.Add(tempIdSprite);
        if (state == 1)
        {         
            GameManager.GM.usedStates.Add(tempIdSprite);
            gameObject.name = sprite.name;
            GameManager.GM.findTxt.text = "Find: " + gameObject.name;
        } 
    }
    public void GetState()
    {
        if (state == 1)
        {
            GameObject tempParticle = Instantiate(particle, gameObject.transform);
            Destroy(tempParticle.gameObject, 1f);
            GameManager.GM.usedSprites.Clear();
            GameManager.GM.levelGame += 1;
            GameManager.GM.cells.Clear();
            StartCoroutine(NextLevel(1f));
        }
        else
        {
            child.transform
                .DOMoveX(transform.position.x, animDuration)
                .SetEase(animEase)
                .From(transform.position.x + 0.5f);
        }
    }
    IEnumerator NextLevel(float time)
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < GameManager.GM.cellParent.childCount; i++)
        {
            Destroy(GameManager.GM.cellParent.GetChild(i).gameObject);
        }
        if (GameManager.GM.levelGame < GameManager.GM.countCell.Length)
            GameManager.GM.CreateCells();
        else if (GameManager.GM.levelGame == GameManager.GM.countCell.Length)
        {
            GameManager.GM.restartMenu.gameObject.SetActive(true);
            GameManager.GM.eclipse.gameObject.SetActive(true);
            GameManager.GM.eclipse.GetComponent<FadeController>().FadeIn(0, 0.6f,GameManager.GM.fadeDuration);
            GameManager.GM.findTxt.gameObject.SetActive(false);
        }
    }
    public void AnimSpawnCell()
    {
        child.transform
            .DOScale(new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z), animDuration)
            .SetEase(animEaseSpawn)
            .From(new Vector3(0, 0, 0));
        Invoke("SpawnSound", 0.1f);
    }
    public void SpawnSound()
    {
        myFx.PlayOneShot(spawnFx);
    }
}
